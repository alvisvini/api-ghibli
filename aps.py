import requests
import json
from logging import debug
from flask import Flask, app
from flask_restful import Resource , Api
from operator import itemgetter

app = Flask(__name__)
api = Api(app)

Filmes = requests.get('https://ghibliapi.herokuapp.com/films').json()
Especies = requests.get('https://ghibliapi.herokuapp.com/species').json()

a = sorted(Filmes, key=itemgetter('rt_score'), reverse=True)
b = sorted(Filmes,key=itemgetter('release_date')) 
d = len(Especies)
x = len(Filmes)
g = []
ano = 0000
resp = 400

class AnoFilme(Resource):
    def get(self,ano):
        g.clear()
        for i in range(0,x):
         if Filmes[i]['release_date'] == ano :
            g.append(Filmes[i])
         
        if not g:
            g.append("Não existe filmes neste ano")

         
        return g

class Top5(Resource):
    def get(self):
        return a[:5]

class oldtop(Resource):
    def get(self):
        return b[:10]        

class sumespe(Resource):
    def get(self):
        return d

class Allf(Resource):
    def get(self):
        return Filmes

class index(Resource):
    def get(salf):
        return resp

api.add_resource(index,'/')
api.add_resource(Allf,'/filmes')
api.add_resource(Top5, '/top5')
api.add_resource(oldtop,'/top10')
api.add_resource(sumespe,'/sumspecies')
api.add_resource(AnoFilme,'/year_launch/<ano>')

if __name__ == "__main__":
   app.run(debug=True,host='0.0.0.0')