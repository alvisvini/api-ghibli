FROM alpine:latest
   
WORKDIR /api-ghibli

RUN  apk add --no-cache python3-dev \
    && apk add py3-pip \
    && pip3 install --upgrade pip

COPY . /api-ghibli

RUN pip3 install -r requirements.txt --ignore-installed six

EXPOSE 5000

ENTRYPOINT ["python3"]

CMD ["aps.py"]
